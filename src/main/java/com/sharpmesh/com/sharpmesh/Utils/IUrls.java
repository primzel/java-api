package com.sharpmesh.com.sharpmesh.Utils;

/**
 * Created by qssim on 7/7/2015.
 */
public interface IUrls {

    //    TODO Path Variable

    String PARAM0 = "/{param0}";
    String PARAM1 = "/{param1}";


    //    TODO URLS
    String URL = "http://mysterious-falls-4691.herokuapp.com";

    String GROUP = "/group";

    String USER = "/user";

    String FEED = "/feed";

    String MEDIA = "/media";

    String GET_MEDIA_BY_ID = MEDIA + PARAM0;

    String USER_CREATE_GROUP = USER + PARAM0 + GROUP;

    String USER_JOIN_GROUP = USER_CREATE_GROUP + PARAM1;


}
