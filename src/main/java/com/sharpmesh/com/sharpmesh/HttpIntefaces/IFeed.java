package com.sharpmesh.com.sharpmesh.HttpIntefaces;

import com.sharpmesh.com.sharpmesh.Utils.IConstants;
import com.sharpmesh.com.sharpmesh.Utils.IUrls;
import com.sharpmesh.com.sharpmesh.domain.Feed;
import com.sharpmesh.com.sharpmesh.domain.Page;
import com.sharpmesh.com.sharpmesh.domain.Response;
import retrofit.http.*;

import java.util.List;

/**
 * Created by qssim on 7/7/2015.
 */
public interface IFeed {

    @Headers({"api-key : " + IConstants.API_KEY, "access-token : " + IConstants.ACCESS_TOKEN})
    @POST(IUrls.FEED)
    Response<Feed> create(@Body Feed feed);

    @GET(IUrls.FEED)
    Page<Feed> list(@Query("pageSize") Integer pageSize,@Query("pageNumber") Integer pageNumber);

}
