package com.sharpmesh.com.sharpmesh.HttpIntefaces;

import com.sharpmesh.com.sharpmesh.Utils.IConstants;
import com.sharpmesh.com.sharpmesh.Utils.IUrls;
import com.sharpmesh.com.sharpmesh.domain.Media;
import com.sharpmesh.com.sharpmesh.domain.Response;
import com.sharpmesh.com.sharpmesh.domain.User;
import retrofit.http.*;
import retrofit.mime.TypedFile;

/**
 * Created by qssim on 7/8/2015.
 */
public interface IMedia {

    @Headers({"api-key : " + IConstants.API_KEY, "access-token : " + IConstants.ACCESS_TOKEN})
    @Multipart
    @POST(IUrls.MEDIA)
    Media upload(@Part("mediaFile")TypedFile mediaFile);

    @GET(IUrls.GET_MEDIA_BY_ID)
    Media get(@Path("param0") String mediaId);

    @POST(IUrls.USER_JOIN_GROUP)
    Response<User> joinGroup(@Path("Param0") String userId,@Path("Param1") String groupId);
}
