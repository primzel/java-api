package com.sharpmesh.com.sharpmesh.HttpIntefaces;

import com.sharpmesh.com.sharpmesh.Utils.IConstants;
import com.sharpmesh.com.sharpmesh.Utils.IUrls;
import com.sharpmesh.com.sharpmesh.domain.Group;
import com.sharpmesh.com.sharpmesh.domain.User;
import com.sharpmesh.com.sharpmesh.domain.Response;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by qssim on 7/7/2015.
 */
public interface IUser {

    @Headers({"api-key : " + IConstants.API_KEY, "access-token : " + IConstants.ACCESS_TOKEN})
    @POST(IUrls.USER)
    Response<User> create(@Body User user);

    @Headers({"api-key : " + IConstants.API_KEY, "access-token : " + IConstants.ACCESS_TOKEN})
    @POST(IUrls.USER_CREATE_GROUP)
    Response<Group> create(@Path("Param0") String userId,@Body Group group);

    @Headers({"api-key : " + IConstants.API_KEY, "access-token : " + IConstants.ACCESS_TOKEN})
    @POST(IUrls.USER_JOIN_GROUP)
    Response<User> joinGroup(@Path("Param0") String userId,@Path("Param1") String groupId);
}
