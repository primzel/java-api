package com.sharpmesh.com.sharpmesh.com.sharpmesh.HttpWrapper;

import com.sharpmesh.com.sharpmesh.HttpIntefaces.IFeed;
import com.sharpmesh.com.sharpmesh.domain.Feed;
import com.sharpmesh.com.sharpmesh.domain.Page;
import com.sharpmesh.com.sharpmesh.domain.Response;

/**
 * Created by qssim on 7/7/2015.
 */
public class PrimzelFeedRestClient extends PrimzelRestClent {

    IFeed feedService = adapter.create(IFeed.class);

    public Feed create(Feed feed) {

        Response<Feed> response = feedService.create(feed);

        return response.getStatus() == 200 ? response.getRecord() : null;
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @return Page<Feed></>
     * */

    public Page<Feed> list(Integer pageNumber, Integer pageSize) {

        return feedService.list(pageSize,pageNumber);
    }
}
