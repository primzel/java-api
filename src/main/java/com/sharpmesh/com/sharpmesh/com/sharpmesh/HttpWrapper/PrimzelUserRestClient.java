package com.sharpmesh.com.sharpmesh.com.sharpmesh.HttpWrapper;

import com.sharpmesh.com.sharpmesh.HttpIntefaces.IUser;
import com.sharpmesh.com.sharpmesh.domain.Group;
import com.sharpmesh.com.sharpmesh.domain.Response;
import com.sharpmesh.com.sharpmesh.domain.User;



/**
 * Created by qssim on 7/7/2015.
 */
public class PrimzelUserRestClient extends PrimzelRestClent{

    private IUser userService=adapter.create(IUser.class);

    public User create(User user){

        Response<User> response=userService.create(user);

        return response.getStatus()==200?response.getRecord():null;
    }

    public Group create(String userId,Group group){
        Response<Group> response=userService.create(userId,group);

        return response.getStatus()==200?response.getRecord():null;
    }

    public User joinGroup(String userId,String groupId){

        Response<User> response=userService.joinGroup(userId, groupId);

        return response.getStatus()==200?response.getRecord():null;
    }
}
