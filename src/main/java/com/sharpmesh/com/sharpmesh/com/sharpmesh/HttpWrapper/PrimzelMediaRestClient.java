package com.sharpmesh.com.sharpmesh.com.sharpmesh.HttpWrapper;

import com.sharpmesh.com.sharpmesh.HttpIntefaces.IMedia;
import com.sharpmesh.com.sharpmesh.domain.Media;
import retrofit.mime.TypedFile;

import java.io.File;

/**
 * Created by qssim on 7/8/2015.
 */
public class PrimzelMediaRestClient extends PrimzelRestClent {

    private static IMedia mediaService = adapter.create(IMedia.class);

    public Media uplaod(File file) {

        TypedFile mediaFile = new TypedFile("multipart/form-data", file);

        return mediaService.upload(mediaFile);
    }

    public Media get(String id) {

        return mediaService.get(id);
    }
}
