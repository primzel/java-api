package com.sharpmesh.com.sharpmesh.domain;

import java.util.List;

/**
 * Created by qssim on 7/7/2015.
 */
public class Page<T> {
    private Integer pageSize;

    private List<T> data;

    private Integer totalPage;

    public Integer getPageSize() {

        return (pageSize == null && data != null) ? data.size() : pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageSize=" + this.getPageSize() +
                ", data=" + data +
                ", totalPage=" + totalPage +
                '}';
    }
}
