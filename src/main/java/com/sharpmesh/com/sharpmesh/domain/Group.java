package com.sharpmesh.com.sharpmesh.domain;

import com.google.gson.annotations.SerializedName;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.Serializable;

/**
 * Created by qssim on 7/8/2015.
 */
public class Group implements Serializable {

    private String title;

    private String description;

    private Boolean status;

    private String userId;

    private String genre;

    private Boolean isDeleted;

    @SerializedName("private")
    private Boolean visable;

    private Boolean joinModeration;

    private String createdDate;

    private String lastModified;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getVisable() {
        return visable;
    }

    public void setVisable(Boolean visable) {
        this.visable = visable;
    }

    public Boolean getJoinModeration() {
        return joinModeration;
    }

    public void setJoinModeration(Boolean joinModeration) {
        this.joinModeration = joinModeration;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "Group{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", userId='" + userId + '\'' +
                ", genre='" + genre + '\'' +
                ", isDeleted=" + isDeleted +
                ", visable=" + visable +
                ", joinModeration=" + joinModeration +
                ", createdDate='" + createdDate + '\'' +
                ", lastModified='" + lastModified + '\'' +
                '}';
    }
}
