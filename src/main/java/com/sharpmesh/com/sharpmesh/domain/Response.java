package com.sharpmesh.com.sharpmesh.domain;

import java.io.Serializable;

/**
 * Created by qssim on 7/7/2015.
 */
public class Response<T> implements Serializable {

    private Integer status;

    private T record;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getRecord() {
        return record;
    }

    public void setRecord(T record) {
        this.record = record;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
