package com.sharpmesh.com.sharpmesh.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by qssim on 7/8/2015.
 */
public class Media implements Serializable {

    private Integer status;

    @SerializedName("_id")
    private String recordId;

    private String url;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @Override
    public String toString() {
        return "Media{" +
                "status=" + status +
                ", recordId='" + recordId + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
