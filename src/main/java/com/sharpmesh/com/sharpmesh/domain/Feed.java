package com.sharpmesh.com.sharpmesh.domain;

import java.io.Serializable;

/**
 * Created by qssim on 7/7/2015.
 */
public class Feed implements Serializable{
    public Feed() {
    }

    private String groupId;

    private String createdByName;

    private String _id;

    private String deviceToken;

    private String userId;

    private String feedMediaUrl;

    private String __v;

    private String createdByEmail;

    private String feedMediaType;

    private String like;

    private String createdDate;

    private String feedText;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFeedMediaUrl() {
        return feedMediaUrl;
    }

    public void setFeedMediaUrl(String feedMediaUrl) {
        this.feedMediaUrl = feedMediaUrl;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public String getCreatedByEmail() {
        return createdByEmail;
    }

    public void setCreatedByEmail(String createdByEmail) {
        this.createdByEmail = createdByEmail;
    }

    public String getFeedMediaType() {
        return feedMediaType;
    }

    public void setFeedMediaType(String feedMediaType) {
        this.feedMediaType = feedMediaType;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFeedText() {
        return feedText;
    }

    public void setFeedText(String feedText) {
        this.feedText = feedText;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "groupId='" + groupId + '\'' +
                ", createdByName='" + createdByName + '\'' +
                ", _id='" + _id + '\'' +
                ", deviceToken='" + deviceToken + '\'' +
                ", userId='" + userId + '\'' +
                ", feedMediaUrl='" + feedMediaUrl + '\'' +
                ", __v='" + __v + '\'' +
                ", createdByEmail='" + createdByEmail + '\'' +
                ", feedMediaType='" + feedMediaType + '\'' +
                ", like='" + like + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", feedText='" + feedText + '\'' +
                '}';
    }
}
