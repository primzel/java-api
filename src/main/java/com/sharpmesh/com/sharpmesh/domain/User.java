package com.sharpmesh.com.sharpmesh.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by qssim on 7/7/2015.
 */
public class User implements Serializable {

    public User() {
        this.contactNumber = "+923244609898";
        this.status = true;
        this.displayImage = "image url";
        this.deviceToken = "dasdasdasdasdasdasqw2423";
//        this.groups = groups;
//        this.isDeleted = isDeleted;
//        this.isActive = isActive;
//        this.createdAt = createdAt;
//        this.updatedAt = updatedAt;
    }

    private String contactNumber;

    private Boolean status;

    private String displayImage;

    private String deviceToken;

    private List<String> groups;

    private Boolean isDeleted;

    private Boolean isActive;

    private String createdAt;

    private String updatedAt;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getDisplayImage() {
        return displayImage;
    }

    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "User{" +
                "contactNumber='" + contactNumber + '\'' +
                ", status=" + status +
                ", displayImage='" + displayImage + '\'' +
                ", deviceToken='" + deviceToken + '\'' +
                ", groups='" + groups + '\'' +
                ", isDeleted=" + isDeleted +
                ", isActive=" + isActive +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
